export enum ENoteActions {
  ADD = "ADD",
  EDIT = "EDIT",
  DELETE = "DELETE",
  VIEW = "VIEW"
}