import { ZodError } from "zod";

export const processZodErrors = (errors: ZodError[]) => {
  return errors.reduce((acc: any, current: any) => {
    const path = current.path[0];

    return {
      ...acc,
      [path]: {
        message: current.message,
        expected: current.expected,
        received: current.received
      },
    };
  }, {});
}