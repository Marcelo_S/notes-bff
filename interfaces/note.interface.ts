export type Note = {
  id: number;
  name: string;
  content: string;
  user_id: string;
  created_at: Date;
  updated_at?: Date | null;
  deleted_at?: Date | null;
}