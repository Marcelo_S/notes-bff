"use client";
import { Note } from "@/interfaces/note.interface";
import { ChangeEvent, Fragment, useEffect, useState } from "react";

type TNoteFormProps = {
  handleChangeInput: (e: ChangeEvent<HTMLInputElement>) => void;
  handleChangeTextArea: (e: ChangeEvent<HTMLTextAreaElement>) => void;
  note: Partial<Note>;
};

const NoteForm = (props: TNoteFormProps) => {
  const { handleChangeInput, handleChangeTextArea, note } = props;

  const onChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    handleChangeInput?.(e);
  };

  const onChangeTextarea = (e: ChangeEvent<HTMLTextAreaElement>) => {
    handleChangeTextArea?.(e);
  };

  return (
    <Fragment>
      <div className="mb-5">
        <label
          htmlFor="name-input"
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
          Name
        </label>
        <input
          type="text"
          id="name-input"
          name="name"
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder="Insert your title note"
          required
          value={note?.name || ""}
          onChange={onChangeInput}
        />
      </div>
      <div className="mb-5">
        <label
          htmlFor="content-message"
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
          Content
        </label>
        <textarea
          id="content-message"
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          required
          onChange={onChangeTextarea}
          value={note?.content || ""}
          name="content"></textarea>
      </div>
    </Fragment>
  );
};

export default NoteForm;
