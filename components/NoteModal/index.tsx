"use client";

import { ChangeEvent, Fragment, ReactNode, useEffect, useState } from "react";
import Modal from "../Modal/Index";
import ModalBackdrop from "../Modal/ModalBackdrop";
import ModalBody, { IModalBody } from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalHeader, { IModalHeaderProps } from "../Modal/ModalHeader";
import { Note } from "@/interfaces/note.interface";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import NoteForm from "../NoteForm";

export interface IModalNoteProps {
  handleSaveNote?: (note: Partial<Note>) => void;
  handleCancelNote?: () => void;
  toggle?: () => void;
  isOpen?: boolean;
  note?: Note;
  title: string;
  showDetails?: boolean;
}

const NoteModal = (props: IModalNoteProps) => {
  const {
    handleCancelNote,
    handleSaveNote,
    toggle,
    isOpen,
    note: noteProps,
    title,
    showDetails,
  } = props;

  const [note, setNote] = useState<Partial<Note>>();

  useEffect(() => {
    setNote(noteProps);
  }, [noteProps]);

  const handleSubmit = () => {
    note && handleSaveNote && handleSaveNote(note);
  };

  const handleCancel = () => {
    handleCancelNote && handleCancelNote();
  };

  const handleChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    setNote((prev) => {
      return {
        ...prev,
        [name]: value as string,
      };
    });
  };

  const handleChangeTextArea = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = e.target;

    setNote((prev) => {
      return {
        ...prev,
        [name]: value as string,
      };
    });
  };

  const renderModalBody = () => {
    if (showDetails) return note?.content;

    return (
      <NoteForm
        note={note as Note}
        handleChangeInput={handleChangeInput}
        handleChangeTextArea={handleChangeTextArea}
      />
    );
  };

  const renderModalFooter = () => {
    if (showDetails) return;

    return (
      <ModalFooter>
        <button
          data-modal-hide="default-modal"
          type="button"
          onClick={handleSubmit}
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
          I accept
        </button>
        <button
          data-modal-hide="default-modal"
          type="button"
          onClick={handleCancel}
          className="py-2.5 px-5 ms-3 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">
          Decline
        </button>
      </ModalFooter>
    );
  };

  return (
    isOpen && (
      <Fragment>
        <ModalBackdrop onClick={toggle} />
        <Modal>
          <ModalHeader>
            <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
              {title}
            </h3>
            <button
              type="button"
              className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
              data-modal-hide="default-modal"
              onClick={toggle}>
              <FontAwesomeIcon icon={faXmark} />
              <span className="sr-only">Close modal</span>
            </button>
          </ModalHeader>
          <ModalBody>{renderModalBody()}</ModalBody>
          {renderModalFooter()}
        </Modal>
      </Fragment>
    )
  );
};

export default NoteModal;
