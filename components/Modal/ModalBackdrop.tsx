import React from 'react';

export interface IModalBackdropProps {
  onClick?: () => void;
}

const ModalBackdrop = (props: IModalBackdropProps) => {
  const { onClick } = props;

  const handleClick = () => {
    onClick && onClick();
  }

  return (
    <div
      className={"fixed inset-0 bg-black bg-opacity-50 transition-opacity opacity-100 z-50"}
      onClick={handleClick}
    />
  );
};

export default ModalBackdrop;
