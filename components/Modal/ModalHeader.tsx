"use client"

import { FC, ReactNode } from "react";

export interface IModalHeaderProps {
  children: ReactNode
}

const ModalHeader: FC<IModalHeaderProps> = (props: IModalHeaderProps) => {
  const { children } = props;

  return (
    <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
      { children }
    </div>
  );
};

export default ModalHeader;
