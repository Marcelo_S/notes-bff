import { FC, ReactNode } from "react";

export interface IModalBody {
  children: ReactNode;
}

const ModalBody: FC<IModalBody> = (props: IModalBody) => {
  const { children } = props;
  return <div className="p-4 md:p-5 space-y-4">{ children }</div>;
};

export default ModalBody;