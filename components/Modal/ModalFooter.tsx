import { FC, ReactNode } from "react";

export interface IModalFooter {
  children: ReactNode;
}

const ModalFooter: FC<IModalFooter> = (props: IModalFooter) => {
  const { children } = props;

  return (
    <div className="flex items-center p-4 md:p-5 border-t border-gray-200 rounded-b dark:border-gray-600">
      {children}
    </div>
  );
};

export default ModalFooter;
