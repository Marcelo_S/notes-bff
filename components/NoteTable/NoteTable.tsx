import { Suspense } from "react";
import NoteTableBody, { INoteTableBodyProps } from "./NoteTableBody";
import NoteTableHead, { INoteTableHeadProps } from "./NoteTableHead";

export interface INoteTableProps {
  tableHead: INoteTableHeadProps;
  tableBody: INoteTableBodyProps;
}

const NoteTable = (props: INoteTableProps) => {
  const { tableHead, tableBody } = props;

  return (
    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
      <NoteTableHead {...tableHead} />
      <NoteTableBody {...tableBody} />
    </table>
  );
};

export default NoteTable;
