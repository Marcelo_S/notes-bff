import { Note } from "@/interfaces/note.interface";
import { ENoteActions } from "@/utils/constants";
import { faEye, faPencil, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export interface INoteTableBodyProps {
  data?: Note[];
  handleDelete?: (id: number) => Promise<void>;
  handleEdit?: (id: number) => Promise<void>;
  handleView?: (id: number) => Promise<void>;
}

interface ActionHandlers {
  [ENoteActions.DELETE]: (id: number) => Promise<void>;
  [ENoteActions.EDIT]: (id: number) => Promise<void>;
  [ENoteActions.VIEW]: (id: number) => Promise<void>;
}


const NoteTableBody = (props: INoteTableBodyProps) => {
  const { data, handleDelete, handleEdit, handleView } = props;

  const onClick = (id: number, action: ENoteActions) => () => {
    if(!handleDelete || !handleEdit || !handleView) return;
    
    const actions: ActionHandlers = {
      [ENoteActions.DELETE]: handleDelete,
      [ENoteActions.EDIT]: handleEdit,
      [ENoteActions.VIEW]: handleView
    }

    const actionFunction = actions[action as keyof ActionHandlers];

    if (!actionFunction) return;
    
    actionFunction(id);
  };

  const renderNotes = () => {
    if (!data?.length) return;

    return data.map((note) => (
      <tr
        key={note.id}
        className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
        <td className="w-4 p-4">{note.id}</td>
        <td
          scope="row"
          className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white text-[14px]">
          {note.name}
        </td>
        <td className="px-6 py-4">{note.content}</td>
        <td className="px-6 py-4 w-1/6">
          <button
            onClick={onClick(note.id, ENoteActions.DELETE)}
            className="text-amber-500 text-[14]px">
            <FontAwesomeIcon icon={faTrash} />
          </button>
          <button
            onClick={onClick(note.id, ENoteActions.EDIT)}
            className="text-lime-600 text-[14]px ml-3">
            <FontAwesomeIcon icon={faPencil} />
          </button>
          <button
            onClick={onClick(note.id, ENoteActions.VIEW)}
            className="text-neutral-50 text-[14]px ml-3">
            <FontAwesomeIcon icon={faEye} />
          </button>
        </td>
      </tr>
    ));
  };

  return <tbody>{renderNotes()}</tbody>;
};

export default NoteTableBody;
