

export enum ESpacing {
  short = "w-4 p-4",
  middle = "px-6 py-3"
}

interface ITableHead {
  title: string,
  spacing: ESpacing
}

export interface INoteTableHeadProps {
  head: ITableHead[]
}

const NoteTableHead = (props: INoteTableHeadProps) => {
  const renderTableHead = () => {
    const tableHead = props.head.map((head) => (
      <th key={head.title} className={head.spacing}>{head.title}</th>
    ));

    return (
      <tr>
        {tableHead}
      </tr>
    )
  }

  return (
    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      {renderTableHead()}
    </thead>
  );
};

export default NoteTableHead;
