import NoteActionAddButton, { INoteActionAddButtonProps } from "./NoteActionAddButton";
import NoteActionSearch from "./NoteActionSearch";

export interface INoteActionProps extends INoteActionAddButtonProps {}

const NoteAction = (props: INoteActionProps) => {
  const { icon, onClick, text } = props;

  return (
    <div className="flex items-center justify-between flex-column flex-wrap md:flex-row space-y-4 md:space-y-0 pb-4 bg-white dark:bg-gray-900 pt-3 px-3">
      <NoteActionAddButton icon={icon} onClick={onClick} text={text}/>
      <NoteActionSearch />
    </div>
  );
};

export default NoteAction;
