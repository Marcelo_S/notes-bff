import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ChangeEvent } from "react";

export interface IProps {
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
  Icon?: JSX.Element;
}

const NoteActionSearch = (props: IProps) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    props.onChange && props.onChange(e);
  };

  const renderIcon = (): JSX.Element => {
    const icon = props.Icon ? (
      props.Icon
    ) : (
      <FontAwesomeIcon icon={faMagnifyingGlass} />
    );
    return (
      <div className="absolute inset-y-0 rtl:inset-r-0 start-0 flex items-center ps-3 pointer-events-none">
        {icon}
      </div>
    );
  };

  const placeholder = props.placeholder ? props.placeholder : "Search Notes";

  return (
    <div className="relative">
      {renderIcon()}
      <input
        type="text"
        id="table-search-users"
        className="block p-2 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        placeholder={placeholder}
        onChange={handleChange}
      />
    </div>
  );
};

export default NoteActionSearch;
