import { faNoteSticky } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export interface INoteActionAddButtonProps {
  onClick?: () => void;
  text?: string;
  icon?: JSX.Element;
}

const NoteActionAddButton = (props: INoteActionAddButtonProps) => {
  const handleClick = () => {
    props.onClick && props.onClick();
  };

  const renderIcon = () => {
    return props.icon ? props.icon : <FontAwesomeIcon icon={faNoteSticky} />;
  };

  const textNote = props.text ? props.text : "Add note";

  return (
    <div>
      <button
        id="dropdownActionButton"
        data-dropdown-toggle="dropdownAction"
        className="inline-flex items-center text-gray-500 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 font-medium rounded-lg text-sm px-3 py-1.5 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
        type="button"
        onClick={handleClick}
      >
        {renderIcon()}
        <span className="ml-3">{ textNote }</span>
      </button>
    </div>
  );
};

export default NoteActionAddButton;
