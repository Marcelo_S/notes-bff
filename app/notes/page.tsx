"use client";

import { Note } from "@/interfaces/note.interface";
import axios from "axios";
import React, { useEffect, useState } from "react";
import NoteAction from "@/components/NoteAction/NoteAction";
import NoteTable from "@/components/NoteTable/NoteTable";
import { ESpacing } from "@/components/NoteTable/NoteTableHead";
import NoteModal from "@/components/NoteModal";
import { ENoteActions } from "@/utils/constants";

const Notes = () => {
  const [notes, setNotes] = useState<Note[]>([]);
  const [note, setNote] = useState<Note>();
  const [modeAction, setModeAction] = useState<ENoteActions>(ENoteActions.VIEW);
  const [isOpen, setIsOpen] = useState(false);

  const fetchData = async () => {
    const {
      data: { body },
    } = await axios.get("/api/notes");

    setNotes(body.data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDeleteNote = async (id: number) => {
    const { data } = await axios.delete(`/api/note/${id}`);
    await fetchData();
  }

  const handleShowNote = async (id: number) => {
    console.log(id);
    const noteData = notes.find((note) => note.id === id);
    setNote(noteData);
    setModeAction(ENoteActions.VIEW);
    toggle();
  }

  useEffect(() => {
    console.log(modeAction);
  }, [modeAction])

  const handleUpdate = async (id: number) => {
    const noteFound = notes.find((note) => note.id === id);
    setNote(noteFound);
    setModeAction(ENoteActions.EDIT);
    toggle();
  }

  const handleSubmit = async (note: Partial<Note>) => {
    if(modeAction === ENoteActions.ADD) {
      await axios.post("/api/notes", note);
    } else {
      await axios.patch(`/api/note/${note.id}`, note);
    }

    await fetchData();
    toggle();
    setNote(undefined);
  }

  const toggle = () => {
    setIsOpen((prev) => !prev);
  }

  const createNote = () => {
    toggle();
    setModeAction(ENoteActions.ADD);
    setNote(undefined);
  }

  const getTitle = () => {
    const text = {
      [ENoteActions.ADD]: "Add new note",
      [ENoteActions.EDIT]: "Edit note",
      [ENoteActions.VIEW]: note?.name || "View note",
    }

    return text[modeAction as keyof typeof text];
  }

  return (
    <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-5/6 mt-8">
      <NoteModal
        handleSaveNote={handleSubmit}
        isOpen={isOpen}
        toggle={toggle}
        note={note}
        title={getTitle()}
        showDetails = {modeAction === ENoteActions.VIEW}
      />
      <NoteAction onClick={createNote}/>
      <NoteTable 
        tableHead={{
          head: [
            {
              title: "ID",
              spacing: ESpacing.short,
            },
            {
              title: "Name",
              spacing: ESpacing.middle,
            },
            {
              title: "Content",
              spacing: ESpacing.middle,
            },
            {
              title: "Action",
              spacing: ESpacing.middle,
            },
          ]
        }}
        tableBody={
          {
            data: notes,
            handleDelete: handleDeleteNote,
            handleEdit: handleUpdate,
            handleView: handleShowNote
          }
        }
      />
      <nav
        className="flex items-center flex-column flex-wrap md:flex-row justify-between pt-4"
        aria-label="Table navigation">
        <span className="text-sm font-normal text-gray-500 dark:text-gray-400 mb-4 md:mb-0 block w-full md:inline md:w-auto">
          Showing
          <span className="font-semibold text-gray-900 dark:text-white">
            1-10
          </span>
          of
          <span className="font-semibold text-gray-900 dark:text-white">
            1000
          </span>
        </span>
        <ul className="inline-flex -space-x-px rtl:space-x-reverse text-sm h-8">
          <li>
            <a
              href="#"
              className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
              Previous
            </a>
          </li>
          <li>
            <a
              href="#"
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
              1
            </a>
          </li>
          <li>
            <a
              href="#"
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
              2
            </a>
          </li>
          <li>
            <a
              href="#"
              aria-current="page"
              className="flex items-center justify-center px-3 h-8 text-blue-600 border border-gray-300 bg-blue-50 hover:bg-blue-100 hover:text-blue-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white">
              3
            </a>
          </li>
          <li>
            <a
              href="#"
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
              4
            </a>
          </li>
          <li>
            <a
              href="#"
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
              5
            </a>
          </li>
          <li>
            <a
              href="#"
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
              Next
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Notes;
