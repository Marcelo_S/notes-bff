import { NextRequest, NextResponse } from "next/server";
import { createClient } from "../../../utils/supabase/server";
import { middlewareValidateNote } from "./middlewares";
import { Note } from "@/interfaces/note.interface";

export const POST = middlewareValidateNote(async (request: NextRequest, note: Note) => {
  const client = createClient();
  const userId = request.headers.get("user-id");
    
  const { error } = await client.from('notes').insert({
    name: note.name,
    content: note.content,
    user_id: userId,
    updated_at: new Date().toISOString(),
  });

  if(error?.message){
    return NextResponse.json({
      status: 500,
      body: { message: error.message },
    });
  }

  return NextResponse.json({
    status: 200,
    body: { message: "Note created successfully!" },
  }); 
})

export const GET = async (request: NextRequest) => {
  const client = createClient();

  const { data: notes, error } = await client.from('notes').select().is('deleted_at', null)

  if(error?.message){
    return NextResponse.json({
      status: 500,
      body: { message: error.message },
    });
  }

  return NextResponse.json({
    status: 200,
    body: { data: notes },
  });
}