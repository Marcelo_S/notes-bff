// middleware.ts

import { Note } from "@/interfaces/note.interface";
import { processZodErrors } from "@/utils/functions";
import { noteSchema } from "@/validation/note.validation";
import { NextRequest, NextResponse } from "next/server";

export const middlewareValidateNote = (handle: (request: NextRequest, note: Note) => Promise<NextResponse>) => async (
    request: NextRequest
  ) => {
    const data = await request.json();
    
    try {
      noteSchema.parse(data);
    } catch (e: any) {
      const errors = processZodErrors(e.errors);
      return NextResponse.json({ errors }, { status: 400 });
    }

  return handle(request, data as Note);
}

