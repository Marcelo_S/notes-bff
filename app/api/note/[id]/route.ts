import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from "next/server";

export const DELETE = async (_request: NextRequest, context: any) => {
  const client = createClient();
  const { id } = context.params;

  const { data, error } = await client
    .from("notes")
    .update({
      deleted_at: new Date().toISOString(),
    })
    .eq("id", id)
    .select();

  if (error) {
    return NextResponse.json({
      status: 500,
      body: { message: "Could not delete note" },
    });
  }

  return NextResponse.json({
    status: 200,
    body: { message: "Note deleted successfully!", data },
  });
};

export const PATCH = async (request: NextRequest, context: any) => {
  const client = createClient();
  const { id } = context.params;
  const note = await request.json();

  const { data: exist } = await client
    .from("notes")
    .select()
    .eq("id", id)
    .is("deleted_at", null);

  if (!exist?.length) {
    return NextResponse.json({
      status: 404,
      body: { message: "Note not found" },
    });
  }

  const { data, error } = await client
    .from("notes")
    .update(note)
    .eq("id", id)
    .is("deleted_at", null)
    .select();

  if (error || !data.length) {
    return NextResponse.json({
      status: 500,
      body: { message: "Could not update note" },
    });
  }

  return NextResponse.json({
    status: 200,
    body: { message: "Note updated successfully!", data },
  });
};

export const GET = async (_request: NextRequest, context: any) => {
  const client = createClient();
  const { id } = context.params;

  const { data: exist } = await client
    .from("notes")
    .select()
    .eq("id", id)
    .is("deleted_at", null)
    .single();

  if (!exist) {
    return NextResponse.json({
      status: 404,
      body: { message: "Note not found" },
    });
  }

  return NextResponse.json({
    status: 200,
    body: { data: exist },
  });
};
