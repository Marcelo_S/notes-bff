import { z } from "zod";

export const noteSchema = z.object({
  name: z.string(),
  content: z.string(),
}).extend({
  error: z.union([z.string(), z.undefined()]).optional(),
});

